#include "videowidget.h"
#include "videocontrol.h"

#include <QApplication>

int main(int argc, char *argv[]) {
    QApplication app(argc, argv);
    gst_init(&argc, &argv);

    auto *videoControl = new VideoControl;

    videoControl->resize(500,500);
    videoControl->show();

    return app.exec();
}
