
#include "videocontrol.h"

#include <QHBoxLayout>
#include <QStyle>
#include <QToolButton>

VideoControl::VideoControl(QWidget *parent) : QWidget(parent) {
    _video = new VideoWidget(this);

    _playButton = new QToolButton(this);
    _playButton->setIcon(style()->standardIcon(QStyle::SP_MediaPlay));
    connect(_playButton, &QAbstractButton::clicked, _video, &VideoWidget::play);

    _pauseButton = new QToolButton(this);
    _pauseButton->setIcon(style()->standardIcon(QStyle::SP_MediaPause));
    connect(_pauseButton, &QAbstractButton::clicked, _video, &VideoWidget::pause);

    _changeButton = new QToolButton(this);
    _changeButton->setIcon(style()->standardIcon(QStyle::SP_ArrowForward));
    connect(_changeButton, &QAbstractButton::clicked, _video, &VideoWidget::changePattern);

    QBoxLayout *btnLayout = new QHBoxLayout;
    btnLayout->addWidget(_playButton);
    btnLayout->addWidget(_pauseButton);
    btnLayout->addWidget(_changeButton);

    QBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(_video);
    layout->addLayout(btnLayout);
    setLayout(layout);

    _video->setPipeline();
}