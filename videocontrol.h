
#ifndef QGST_VIDEOCONTROL_H
#define QGST_VIDEOCONTROL_H

#include "videowidget.h"

#include <QAbstractButton>
#include <QTime>
#include <QWidget>

class VideoControl : public QWidget {
Q_OBJECT
public:
    explicit VideoControl(QWidget *parent = nullptr);

private:
    QAbstractButton *_playButton = nullptr;
    QAbstractButton *_pauseButton = nullptr;
    QAbstractButton *_changeButton = nullptr;

    VideoWidget *_video = nullptr;
};


#endif //QGST_VIDEOCONTROL_H
