
#include "videowidget.h"

#include <QDebug>
#include <QGuiApplication>
#include <QKeyEvent>
#include <QRandomGenerator>

#include <qpa/qplatformnativeinterface.h>

#define VID_SRC_NAME "video_source"
#define VID_SINK_NAME "video_sink"

GstBusSyncReply bus_sync_handler(GstBus *bus, GstMessage *message, gpointer user_data) {
    auto *v = static_cast<VideoWidget *>(user_data);
    if (gst_is_video_overlay_prepare_window_handle_message(message)) {
        GstVideoOverlay *videoOverlay = GST_VIDEO_OVERLAY (GST_MESSAGE_SRC(message));
        gst_video_overlay_set_window_handle(videoOverlay, (guintptr) v->winId());
        gst_video_overlay_set_render_rectangle(videoOverlay, v->x(), v->y(), v->width(), v->height());
        v->videoOverlay = videoOverlay;
        goto drop;
    }

    return GST_BUS_PASS;

    drop:
    gst_message_unref(message);
    return GST_BUS_DROP;
}

GstPadProbeReturn pad_probe_cb (GstPad * pad, GstPadProbeInfo * info, gpointer user_data)
{
    GstElement *pipeline = static_cast<GstElement *>(user_data);
    GstElement *vidsrc = gst_bin_get_by_name(GST_BIN (pipeline), VID_SRC_NAME);

    GST_DEBUG_OBJECT (pad, "pad is blocked now");

    /* remove the probe first */
    gst_pad_remove_probe (pad, GST_PAD_PROBE_INFO_ID (info));

    /* change pattern */
    int randomPattern = QRandomGenerator::global()->bounded(0, 25);
    g_object_set(vidsrc, "pattern", randomPattern, nullptr);

    gst_object_unref (vidsrc);
    return GST_PAD_PROBE_OK;
}

GstPadProbeReturn on_video_sink_data_flow (GstPad * pad, GstPadProbeInfo * info, gpointer user_data)
{
    auto mini_obj = static_cast<GstMiniObject *>(GST_PAD_PROBE_INFO_DATA (info));
    auto timer = static_cast<QElapsedTimer *>(user_data);
    if (GST_IS_BUFFER (mini_obj)) {
        /* remove the probe */
        gst_pad_remove_probe (pad, GST_PAD_PROBE_INFO_ID (info));
        qDebug() << "Elapsed time: " << timer->elapsed() << " ms";
    }
    return GST_PAD_PROBE_OK;
}

VideoWidget::VideoWidget(QWidget *parent) : QWidget(parent) {
    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
}

VideoWidget::~VideoWidget() {
    _destroyPipeline();
}

void VideoWidget::setPipeline() {
    _destroyPipeline();
    QString pipelineStr = QString("videotestsrc name=%1 ! autovideosink name=%2").arg(VID_SRC_NAME, VID_SINK_NAME);
    GST_ERROR("Pipeline: %s", pipelineStr.toStdString().c_str());
    _pipeline = gst_parse_launch(pipelineStr.toStdString().c_str(), NULL);
    GstBus *bus = gst_pipeline_get_bus(GST_PIPELINE (_pipeline));
    gst_bus_add_signal_watch(bus);
    gst_bus_set_sync_handler(bus, bus_sync_handler, this, NULL);
    gst_object_unref(bus);
}

void VideoWidget::play() {
    _setState(GST_STATE_PLAYING);
}

void VideoWidget::pause() {
    _setState(GST_STATE_PAUSED);
}

void VideoWidget::stop() {
    _setState(GST_STATE_NULL);
}

void VideoWidget::changePattern() {
    if (!_elapsedTimer)
        _elapsedTimer = new QElapsedTimer;
    _elapsedTimer->start();

    // https://gstreamer.freedesktop.org/documentation/application-development/advanced/pipeline-manipulation.html#changing-elements-in-a-pipeline
    GstElement *vidsrc = gst_bin_get_by_name(GST_BIN (_pipeline), VID_SRC_NAME);
    GstPad *blockpad = gst_element_get_static_pad(vidsrc, "src");
    gst_pad_add_probe (blockpad, GST_PAD_PROBE_TYPE_BLOCK_DOWNSTREAM,
                       pad_probe_cb, _pipeline, NULL);

    // Data probe on videosink
    GstElement *videoSink = gst_bin_get_by_name(GST_BIN (_pipeline), VID_SINK_NAME);
    GstPad *pad = gst_element_get_static_pad(videoSink, "sink");
    gst_pad_add_probe(pad, GST_PAD_PROBE_TYPE_DATA_BOTH, on_video_sink_data_flow, _elapsedTimer, NULL);

    gst_object_unref(blockpad);
    gst_object_unref(vidsrc);
}

bool VideoWidget::event(QEvent *event) {
    /* Play video automatically at the first time window is active */
    if (event->type() == QEvent::WindowActivate) {
        if (_firstActive) {
            play();
            _firstActive = false;
        }
    }
    return QWidget::event(event);
}

void VideoWidget::resizeEvent(QResizeEvent *event) {
    QWidget::resizeEvent(event);
    if (videoOverlay)
        gst_video_overlay_set_render_rectangle(videoOverlay, x(), y(), width(), height());
}

void VideoWidget::_destroyPipeline() {
    if (_pipeline) {
        stop();
        g_object_unref(_pipeline);
        _pipeline = nullptr;
    }
}

void VideoWidget::_setState(GstState state) {
    if (_pipeline)
        gst_element_set_state(_pipeline, state);
}